# Makefile for Golang project with Docker Compose
include app.env

# Variables
BINARY_NAME=smsgw-api
DOCKER_COMPOSE_FILE=docker-compose.yml

# Targets
.PHONY: all build run clean

all: build

build:
	@echo "Building $(BINARY_NAME)..."
	@go build -o $(BINARY_NAME) ./cmd/sms-gw

run:
	@echo "Running $(BINARY_NAME)..."
	@go run ./cmd/sms-gw

clean:
	@echo "Cleaning up..."
	@rm -f $(BINARY_NAME)
